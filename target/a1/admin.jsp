<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
    <form action = "AdminPage" method="post">
        <h2>Post & update a flight:</h2>
        <p>Flight number:</p>
        <input type="text" name="number"/><br>
        <p>Airplane type:</p>
        <input type="text" name="type"/><br>
        <p>Departure city:</p>
        <input type="text" name="departCity"/><br>
        <p>Departure time:</p>
        <input type="datetime-local" name="departTime"/><br>
        <p>Arrival city:</p>
        <input type="text" name="arrivCity"/><br>
        <p>Arrival time:</p>
        <input type="datetime-local" name="arrivTime"/><br><br>
        <input type="submit" value="submit" name="button">
        <input type="submit" value="update" name="button">

        <h2>Delete:</h2>
        <p>Insert flight number to delete: <input type="text" name="flightNumber"/></p>
        <input type="submit" value="delete" name="button">
        <h2>View details of a flight:</h2>
        <input type="submit" value="view" name="button"><br><br>
        <input type="submit" value="logOut" name="button">
    </form>
</body>
</html>
