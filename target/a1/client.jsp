<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Client</title>
</head>
<body>
    <form action="ClientPage" method="post">
        <h2>View all flights that are available:</h2>
        <input type="submit" value="view" name="clientButton">

        <h2>Insert coordinates to find local time:</h2>
        <p>Latitude:</p>
        <input type="text" name="lat"/><br>
        <p>Longitude:</p>
        <input type="text" name="long"/><br><br>
        <input type="submit" value="findOut" name="clientButton"><br><br>
        <input type="submit" value="logOut" name="button">
    </form>
</body>
</html>
