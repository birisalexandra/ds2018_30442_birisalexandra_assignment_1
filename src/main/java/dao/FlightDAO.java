package dao;

import helpers.HibernateConfig;
import entity.Flight;
import org.hibernate.*;

import java.util.List;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;


public class FlightDAO {

    public Flight addFlight(Flight flight) {
        int flightId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setFlightId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return flight;
    }

    public void deleteFlight(int number)
    {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("delete FROM Flight WHERE number = :number");
            query.setParameter("number", number);
            query.executeUpdate();
            tx.commit();
        }  catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
    }

    public void updateFlight(Flight flight)
    {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            Flight persisted = findFlight(flight.getFlightNumber());
            persisted.setAirplaneType(flight.getAirplaneType());
            persisted.setDepartureCity(flight.getDepartureCity());
            persisted.setDepartureTime(flight.getDepartureTime());
            persisted.setArrivalCity(flight.getArrivalCity());
            persisted.setArrivalTime(flight.getArrivalTime());
            session.update(persisted);
            tx.commit();
        }  catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public Flight findFlight(int number) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE number = :number");
            query.setParameter("number", number);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    @SuppressWarnings("unchecked")
    public List<Flight> findFlights() {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return flights;
    }

}
