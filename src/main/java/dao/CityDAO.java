package dao;

import helpers.HibernateConfig;
import entity.City;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class CityDAO {

    public City addCity(City city) {
        int cityId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            cityId = (Integer) session.save(city);
            city.setCityId(cityId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return city;
    }

    public City findCity(String name) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        City city= null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :n");
            query.setParameter("n", name);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return city;
    }
}
