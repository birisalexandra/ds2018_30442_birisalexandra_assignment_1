package dao;

import helpers.HibernateConfig;
import entity.User;
import org.hibernate.*;

import static jdk.nashorn.internal.objects.NativeError.printStackTrace;

public class UserDAO {

    public User findUser(String username, String password) {
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :u AND password = :p");
            query.setParameter("u", username);
            query.setParameter("p", password);
            user = (User) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return user;
    }

    public User addUser(User user) {
        int userId = -1;
        Session session = HibernateConfig.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            user.setUserId(userId);
            tx.commit();
        } catch (HibernateException e) {
            printStackTrace(e);
        } finally {
            session.close();
        }
        return user;
    }
}
