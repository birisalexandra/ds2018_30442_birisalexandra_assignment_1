package business.servlets;

import dao.FlightDAO;
import entity.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/Flights")
public class FlightsServlet extends HttpServlet {
    private FlightDAO flightDAO;

    public void init() {
        this.flightDAO = new FlightDAO();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Flight> flights = flightDAO.findFlights();
        PrintWriter out = response.getWriter();
        out.write("<html><body>");
        out.write("<h2>All flights</h2>");
        out.write("<table><tr>" +
                "<th>Flight number</th>" +
                "<th>Airplane Type</th>" +
                "<th>Departure city</th>" +
                "<th>Departure time</th>" +
                "<th>Arrival city</th>" +
                "<th>Arrival time</th></tr>");
        for(Flight flight: flights)
            out.write("<tr><td>" + flight.getFlightNumber()
                    + "</td><td>" + flight.getAirplaneType() +
                    "</td><td>" + flight.getDepartureCity().getName() +
                    "</td><td>" + flight.getDepartureTime() +
                    "</td><td>" + flight.getArrivalCity().getName() +
                    "</td><td>"+ flight.getArrivalTime() +
                    "</td></tr>");
        out.write("</table>");
        out.write("</div></body></html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
