package business.servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entity.City;
import entity.Flight;
import entity.Role;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@WebServlet("/AdminPage")
public class AdminServlet extends HttpServlet {
    private CityDAO cityDAO;
    private FlightDAO flightDAO;

    public void init() {
        this.cityDAO = new CityDAO();
        this.flightDAO = new FlightDAO();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession(true);
        if (httpSession.getAttribute("userType") == Role.ADMIN) {
            RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/admin.jsp");
            rd.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath());;
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/admin.jsp");
        switch(request.getParameter("button")) {
            case "submit":
                flightDAO.addFlight(getInputFlight(request));
                rd.forward(request, response);
                break;
            case "delete":
                flightDAO.deleteFlight(Integer.parseInt(request.getParameter("flightNumber")));
                rd.forward(request, response);
                break;
            case "update":
                flightDAO.updateFlight(getInputFlight(request));
                rd.forward(request, response);
                break;
            case "view":
                response.sendRedirect(request.getContextPath() + "/Flights");
                break;
            case "logOut":
                HttpSession httpSession = request.getSession(true);
                httpSession.setAttribute("userType", "");
                response.sendRedirect(request.getContextPath());
                break;
        }
    }

    private Flight getInputFlight(HttpServletRequest request) {
        Integer number = Integer.parseInt(request.getParameter("number"));
        String type = request.getParameter("type");
        City departCity = cityDAO.findCity(request.getParameter("departCity"));
        Timestamp departTime = Timestamp.valueOf(LocalDateTime.parse(request.getParameter("departTime")));
        City arrivCity = cityDAO.findCity(request.getParameter("arrivCity"));
        Timestamp arrivTime = Timestamp.valueOf(LocalDateTime.parse(request.getParameter("arrivTime")));

        return new Flight(0, number, type, departCity, departTime, arrivCity, arrivTime);
    }
}
