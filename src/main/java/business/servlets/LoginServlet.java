package business.servlets;

import dao.UserDAO;
import entity.Role;
import entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("")
public class LoginServlet extends HttpServlet {
    private UserDAO userDAO;

    public void init() throws ServletException {
        this.userDAO = new UserDAO();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = userDAO.findUser(request.getParameter("username"), request.getParameter("password"));
        HttpSession httpSession = request.getSession(true);
        if (user == null) {
            response.sendRedirect(request.getContextPath());
        }
        else {
            if (user.getRole() == Role.CLIENT) {
                httpSession.setAttribute("userType", Role.CLIENT);
                response.sendRedirect(request.getContextPath() + "/ClientPage");
            }
            else {
                httpSession.setAttribute("userType", Role.ADMIN);
                response.sendRedirect(request.getContextPath() + "/AdminPage");
            }
        }
    }
}
