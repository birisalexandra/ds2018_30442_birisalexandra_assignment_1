package business.servlets;

import entity.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

@WebServlet("/ClientPage")
public class ClientServlet extends HttpServlet {

    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession(true);
        if (httpSession.getAttribute("userType") == Role.CLIENT) {
            RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/client.jsp");
            rd.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath());
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getParameter("clientButton")) {
            case "view":
                response.sendRedirect(request.getContextPath() + "/Flights");
                break;
            case "findOut":
                String urlString = String.format("http://new.earthtools.org/timezone/%f/%f", Float.parseFloat(request.getParameter("lat")), Float.parseFloat(request.getParameter("long")));
                String localTime = null;

                try {
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document document = db.parse(new URL(urlString).openConnection().getInputStream());
                    Element root = document.getDocumentElement();
                    localTime = root.getElementsByTagName("localtime").item(0).getTextContent();
                } catch (ParserConfigurationException | SAXException e) {
                    e.printStackTrace();
                }

                PrintWriter out = response.getWriter();
                out.write("<html><body>");
                out.write("<h2>Local time for the searched city is: <h2>");
                out.write("<p>" + localTime + "</p>");
                out.write("</body></html>");
                break;
            case "logOut":
                HttpSession httpSession = request.getSession(true);
                httpSession.setAttribute("userType", "");
                response.sendRedirect(request.getContextPath());
                break;
        }
    }
}
