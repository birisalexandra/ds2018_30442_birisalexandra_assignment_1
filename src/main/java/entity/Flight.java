package entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="flight")
public class Flight {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "flight_id")
    private Integer flightId;

    @Column(name="number")
    private Integer flightNumber;

    @Column(name="type")
    private String airplaneType;

    @ManyToOne
    @JoinColumn(name = "depart_id")
    private City departureCity;

    @Column(name="depart_time")
    private Timestamp departureTime;

    @ManyToOne
    @JoinColumn(name = "arriv_id")
    private City arrivalCity;

    @Column(name="arriv_time")
    private Timestamp arrivalTime;

    public Flight() {}

    public Flight(Integer id, Integer flightNumber, String airplaneType, City departureCity, Timestamp departureTime, City arrivalCity, Timestamp arrivalTime) {
        this.flightId = id;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureTime = departureTime;
        this.arrivalCity = arrivalCity;
        this.arrivalTime = arrivalTime;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
